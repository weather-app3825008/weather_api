# Start the app
import os

from api.app import app

if __name__ == '__main__':
    app.run(debug=os.environ.get('FLASK_ENV', 'development') == 'development', host='0.0.0.0',
            port=int(os.environ.get("PORT", 5000)))
