# About Project

This is a server based on flask and using vercel for deployment

## Running the app

`python -m venv venv`

`source venv/bin/activate`

`pip install requirements.txt`

`python main.py`
Server is accessible at http://127.0.0.1:5000/

## Accessing the weather api

 E.g http://127.0.0.1:5000/weather/zip1,zip2,zip3