import requests
from flask_cors import cross_origin


def get_weather_by_zip(api_key, zipcode, country_code='in'):
    """
    Get weather details from open weather api
    :param api_key:
    :param zipcode:
    :param country_code:
    :return:
    """
    base_url = "https://api.openweathermap.org/data/2.5/weather?"
    complete_url = f"{base_url}zip={zipcode},{country_code}&appid={api_key}"

    response = requests.get(complete_url)
    data = response.json()
    # Update the icon to actual url
    for item in data['weather']:
        item['icon'] = f"http://openweathermap.org/img/w/{item['icon']}.png"
    data['zipcode'] = zipcode
    return data


def create_weather_api(app, api_key : str):
    @app.route('/weather/<path:zipcodes>')
    @cross_origin()
    def get_weather(zipcodes: str):
        zipcodes = zipcodes.split(",")
        return [get_weather_by_zip(api_key=api_key, zipcode=zipcode)
                for zipcode in zipcodes]
