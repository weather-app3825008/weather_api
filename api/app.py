import os

from flask import Flask

from api.weather import create_weather_api
from flask_cors import CORS

app = Flask(__name__)

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

create_weather_api(app, os.environ.get('API_KEY', 'b13c0f79830789aa844ef77f2c44888d'))